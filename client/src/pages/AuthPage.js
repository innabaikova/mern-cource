import React, { useState, useEffect, useContext } from "react"
import { useHttp } from "../hooks/http.hook"
import { useMessage } from "../hooks/message.hook"
import { AuthContext } from "../context/AuthContext"

export const AuthPage = () => {
    const auth = useContext(AuthContext)
    const { loading, request, error, clearError } = useHttp()
    const message = useMessage()

    const [form, setForm] = useState({
        email: "",
        password: "",
    })

    useEffect(() => {
        message(error)
       clearError()
    }, [error, message, clearError])

    useEffect(() => {
        window.M.updateTextFields()
    }, [])

    const changeHandler = (event) => {
        const { name, value } = event.target
        setForm({ ...form, [name]: value })
    }

    const registerHandler = async () => {
        try {
            const data = await request("api/auth/register", "POST", form)
            message(data)
        } catch (e) {}
    }

    const loginHandler = async () => {
        try {
            const data = await request("api/auth/login", "POST", form)
            auth.login(data.token, data.userId)
        } catch (e) {}
    }

    return (
        <div className='row'>
            <div className='col s6 offset-s3'>
                <h1>Shorten link</h1>
                <div className='card blue darken-1'>
                    <div className='card-content white-text'>
                        <span className='card-title'>Authorization</span>
                        <div className='input-field'>
                            <input
                                placeholder='Enter email'
                                id='email'
                                type='text'
                                name='email'
                                className='yellow-input'
                                value={form.email}
                                onChange={changeHandler}
                            />
                            <label htmlFor='email'>Email</label>
                        </div>
                        <div className='input-field'>
                            <input
                                placeholder='Enter password'
                                id='password'
                                type='password'
                                name='password'
                                className='yellow-input'
                                value={form.password}
                                onChange={changeHandler}
                            />
                            <label htmlFor='password'>Password</label>
                        </div>
                    </div>
                    <div className='card-action'>
                        <button
                            className='btn yellow darken-4'
                            onClick={loginHandler}
                            style={{ marginRight: 10 }}
                            disabled={loading}>
                            Sign In
                        </button>
                        <button
                            className='btn grey lighten-1 black-text'
                            onClick={registerHandler}
                            disabled={loading}>
                            Register
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}
